
public class ListNode {
	private Object value;
	private ListNode next;
	
	public ListNode(Object v, ListNode n){
		value = v;
		next = n;
	}
	//accessors
	public Object getValue(){return value;}
	public ListNode getNext(){return next;}
	//mutators
	public void setValue(Object v){value = v;}
	public void setNext(ListNode n){next = n;}
}
