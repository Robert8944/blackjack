


public class CircularLinkedList {
	//maternity ward
	private ListNode last;
	
	//constructor
	CircularLinkedList(){last = null;}
	
	//checks if its empty
	public boolean isEmpty(){return last==null;}
	
	public void add(Object toAdd){
		if(isEmpty()){
			last = new ListNode(toAdd, null);
			last.setNext(last);	
		}
		else{
			last.setNext(new ListNode(toAdd,last.getNext()));
			last = last.getNext();
		}
	}
	public String toString(){
		if(isEmpty()){
			return "empty";
		}
		String s = "";
		ListNode current = last.getNext();
		while(current != last){
			s += current.getValue()+"\n";
			//s += " ";
			current = current.getNext();
		}
		s += current.getValue();
		return s;
	}
	public int size(){
		if(isEmpty()){
			return 0;
		}
		int s = 0;
		ListNode current = last.getNext();
		while(current != last){
			s += 1;
			current = current.getNext();
		}
		s += 1;
		return s;
	}
	public void addFirst(Object toAdd){
		if(isEmpty()){
			last = new ListNode(toAdd, null);
			
		}
		else{
			last.setNext(new ListNode(toAdd,last.getNext()));
			
		}
	}
	public Object removeFirst(){
		if(isEmpty()){
			return null;
		}
		ListNode dead = last.getNext();
		if(last.getNext()==last){
			last=null;
		}
		else{
			last.setNext(last.getNext().getNext());
		}
		return dead.getValue();
	}
	public Object Get(int i){
		if(isEmpty()){
			return null;
		}
		int s = 0;
		ListNode current = last.getNext();
		while(s != i){
			s += 1;
			current = current.getNext();
		}
		
		return current.getValue();
		
	
	}
	public Object Set(int i, Object O){
		if(isEmpty()){
			return null;
		}
		int s = 0;
		ListNode current = last.getNext();
		while(s != i){
			s += 1;
			current = current.getNext();
		}
		
		Object ans = current.getValue();
		current.setValue(O);
		return ans;
	}
	public void add(int i, Object O){
		if(isEmpty()||i==0){
			this.addFirst(O);
			return;
		}
		if(i == size()){
			last.setNext(new ListNode(O,last.getNext()));
			last = last.getNext();
		}
		else{
			int s = 0;
			ListNode current = last.getNext();
			while(s != i-1){
				s ++;
				current = current.getNext();
			}
			
			current.setNext(new ListNode(O,current.getNext()));	
		}
	}
	public Object Remove(int i){
		int sz = size();
		
		if(size()==0){
			return null;
		}
		if(size()==1){
			Object ans = last.getValue();
			last = null;
			return ans;
		}
		
		ListNode previous = last.getNext();
		for(int x=0; x<i-1; x++)
			previous = previous.getNext();			
		
		Object ans = previous.getNext().getValue();
		previous.setNext(previous.getNext().getNext());
		
		//if you're removing the last dude
		if(i == sz-1){
			
			last = previous;
		}
		return ans;
	}
}
