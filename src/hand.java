import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;


public class hand extends CircularLinkedList{
	
	protected Image imgOfBack;
	private boolean down;
	public hand(boolean d){
		down=d;
		imgOfBack = Toolkit.getDefaultToolkit().getImage("b1fv.png");
		MediaTracker mt = new MediaTracker(new Component(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;});
		mt.addImage(imgOfBack,0);
		try{mt.waitForAll();}catch(Exception ex){ex.printStackTrace();}
	}
	public void addCard(Card toAdd){
		this.add(toAdd);
	}
	public void emptyHand(){
		while(!isEmpty()){
			this.removeFirst();
		}
	}
	public int getValue(){
		int ans=0;
		int Na=0;
		
		for(int i=0; i<this.size(); i++){		
			Card simp = (Card)(this.Get(i));			
			
			if (simp.getPoints()==1)
				Na++;
			else
				ans+=simp.getPoints();
		}
		if(Na!=0 && Na>1){
			ans+=Na-1;
			if (ans+11 < 21)
				ans+=11;
			else
				ans+=1;
		}
		if(Na==1){
			if (ans+11 < 21)
				ans+=11;
			else
				ans+=1;
		}
		return ans;
	}
	public void draw(Graphics g, int i, int j) {
		if (down){
			for(int loop=0; loop<this.size(); loop++){
				((Card)(this.Get(loop))).draw(g,i+50*loop,j);
			}
		}
		else if (!down){
			for(int loop=0; loop<this.size(); loop++){
				if(loop!=0)
					((Card)(this.Get(loop))).draw(g,i+50*loop,j);
				else
					g.drawImage(imgOfBack, i, j, 90, 90, null, null);
			}
		}
		
	}
	public void setDown(){
		down = false;
	}
	public void setUp(){
		down = true;
	}

}
