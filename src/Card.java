import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;


public class Card {
	protected int val;
	protected String suit;
	protected Image img;
	
	
	
	public Card(int v, String s){
		val=v;
		suit=s;
	}
	public Card(int v, String s, String s2){
		val=v;
		suit=s;
		img = Toolkit.getDefaultToolkit().getImage(s2);
		MediaTracker mt = new MediaTracker(new Component(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;});
		mt.addImage(img,0);
		try{mt.waitForAll();}catch(Exception ex){ex.printStackTrace();}
	}
	public Card(int v, String s, Image i){
		val=v;
		suit=s;
		img = i;
	}
	 public int getPoints(){
		int ans = val;
		if (ans>10){
			ans=10;
		}
		return ans;
	}
	 public String toString (){
		 String ans="";
		 if (suit.equals("Hearts")){
			 if (val == 1){
				 ans="Ace of Hearts";
			 }
			 else if (val == 11){
				 ans="Jack of Hearts";
			 }
			 else if (val == 12){
				 ans="Queen of Hearts";
			 }
			 else if (val == 13){
				 ans="King of Hearts";
			 }
			 else{
				 ans= val+" of Hearts";
			 }
		 }
		 else if (suit.equals("Diamonds")){
			 if (val == 1){
				 ans="Ace of Diamonds";
			 }
			 else if (val == 11){
				 ans="Jack of Diamonds";
			 }
			 else if (val == 12){
				 ans="Queen of Diamonds";
			 }
			 else if (val == 13){
				 ans="King of Diamonds";
			 }
			 else{
				 ans= val+" of Diamonds";
			 }
		 }
		 else if (suit.equals("Clubs")){
			 if (val == 1){
				 ans="Ace of Clubs";
			 }
			 else if (val == 11){
				 ans="Jack of Clubs";
			 }
			 else if (val == 12){
				 ans="Queen of Clubs";
			 }
			 else if (val == 13){
				 ans="King of Clubs";
			 }
			 else{
				 ans= val+" of Clubs";
			 }
			 
		 }
		 else if (suit.equals("Spades")){
			 if (val == 1){
				 ans="Ace of Spades";
			 }
			 else if (val == 11){
				 ans="Jack of Spades";
			 }
			 else if (val == 12){
				 ans="Queen of Spades";
			 }
			 else if (val == 13){
				 ans="King of Spades";
			 }
			 else{
				 ans= val+" of Spades";
			 }
		 }
		 return ans;
	 }
	public void draw(Graphics g, int i, int j) {
		g.drawImage(img, i, j, 90, 90, null, null);
		
	}
}
