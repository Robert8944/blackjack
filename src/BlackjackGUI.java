import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.*;

public class BlackjackGUI extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	public deck crds;
	public hand dealersHand, yourHand;
	public JButton hit, stay, reset, advice;
	public JPanel BackgroundColoring, setUp, setUp2, spaceFiller, spaceFiller2;
	private Font myFont=new Font("Arial",Font.BOLD, 53);

	
	//constructor
		public BlackjackGUI(){
			super("Blackjack");
			//do stuff
			Image i = Toolkit.getDefaultToolkit().getImage("Cursor.png");
			this.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(i, new Point(0,0), "name"));
			BackgroundColoring = new JPanel();
			BackgroundColoring.setBackground(Color.GREEN);
			this.add(BackgroundColoring);
			dealersHand = new hand(false);
			yourHand = new hand(true);
			crds = new deck();
			advice = new JButton("advice");
			advice.addActionListener(this);
			advice.setBackground(Color.GRAY);
			reset = new JButton("reset");
			reset.addActionListener(this);
			reset.setBackground(Color.GRAY);
			stay = new JButton("stay");
			stay.addActionListener(this);
			stay.setBackground(Color.GRAY);
			hit = new JButton("hit");
			hit.addActionListener(this);
			hit.setBackground(Color.GRAY);
			setUp = new JPanel();
			setUp2 = new JPanel();
			spaceFiller = new JPanel();
			spaceFiller2 = new JPanel();
			spaceFiller.setBackground(Color.GREEN);
			spaceFiller2.setBackground(Color.GREEN);
			yourHand.add(crds.deal());
			yourHand.add(crds.deal());
			dealersHand.add(crds.deal());
			dealersHand.add(crds.deal());
			setUp.setLayout(new GridLayout(4,1));
			setUp2.setLayout(new GridLayout(1,3));
			setUp2.add(spaceFiller);
			setUp.add(advice);
			setUp.add(hit);
			setUp.add(stay);
			setUp.add(reset);
			setUp2.add(setUp);
			setUp2.add(spaceFiller2);
			this.add(setUp2,BorderLayout.NORTH);
			
			//finish up
			this.setSize(500,500);
			this.setVisible(true);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
		}
		
	//steal the paint function
	
	public void paint(Graphics g){
		super.paint(g);
		
		g.setColor(Color.WHITE);
		g.setFont(myFont);
		g.drawString("Dealer",10,200);
		g.drawString("You:"+yourHand.getValue(),10,465);
		
		yourHand.draw(g,200,400);
		
		dealersHand.draw(g,200,150);
	}
	
	
	
	public static void main(String[] args){
		new BlackjackGUI();	
	}

	
	
	public void newGame(){
		dealersHand.emptyHand();
		yourHand.emptyHand();
		crds.getNew();
		yourHand.add(crds.deal());
		yourHand.add(crds.deal());
		dealersHand.add(crds.deal());
		dealersHand.add(crds.deal());
		dealersHand.setDown();
	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource().equals(advice)){
			if (yourHand.getValue()<=16){
				JOptionPane.showMessageDialog(this,"I'd hit that.");
			}
			else{
				JOptionPane.showMessageDialog(this,"I'd stay here.");
			}
		}
		if(arg0.getSource().equals(reset)){
			this.newGame();
		}
		if(arg0.getSource().equals(hit)){
			yourHand.add(crds.deal());
			if (yourHand.getValue()==21){
				this.repaint();
				JOptionPane.showMessageDialog(this, "BLACKJACK! YOU WIN!!");
				this.newGame();
				this.repaint();
				return;
			}
			if (yourHand.getValue()>21){
				dealersHand.setUp();
				this.repaint();
				JOptionPane.showMessageDialog(this, "BUST!!  Final score:   you-"+yourHand.getValue()+"  dealer-"+dealersHand.getValue());
				this.newGame();
				this.repaint();
				return;
			}
		}
		if(arg0.getSource().equals(stay)){
			dealersHand.setUp();
			while (dealersHand.getValue()<17){
				dealersHand.add(crds.deal());
				this.repaint();
			}
			if (dealersHand.getValue()>21){
				JOptionPane.showMessageDialog(this, "Dealer Bust, You Win!!  Final score:   you-"+yourHand.getValue()+"  dealer-"+dealersHand.getValue());
				
				this.newGame();
				this.repaint();
				return;
			}
			else if (yourHand.getValue()>dealersHand.getValue()){
				JOptionPane.showMessageDialog(this, "You Win!!  Final score:   you-"+yourHand.getValue()+"  dealer-"+dealersHand.getValue());
				this.newGame();
				this.repaint();
				return;
			}
			else if (yourHand.getValue()==dealersHand.getValue()){
				JOptionPane.showMessageDialog(this, "You Tie!!  Final score:   you-"+yourHand.getValue()+"  dealer-"+dealersHand.getValue());
				this.newGame();
				this.repaint();
				return;
			}
			else if (yourHand.getValue()<=dealersHand.getValue()){
				JOptionPane.showMessageDialog(this, "You Loose!!  Final score:   you-"+yourHand.getValue()+"  dealer-"+dealersHand.getValue());
				this.newGame();
				this.repaint();
				return;
			}
		}
		this.repaint();
		
	}
}